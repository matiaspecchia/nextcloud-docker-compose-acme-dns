# Nextcloud in Docker Compose with automatic SSL via DNS challenge

Nexcloud deployment for multiple archs adapted for bein used with
Gandi Live DNS API.


# Installation

Do
	git submdule update --init

Then see [current-compose/README.md](current-compose/README.md) for instructions.

# File Organization

.
- `README.md:` this file
- `current-compose/`: Docker-compose with necessary docker files. Details shown below.
- `install_nextcloud.yml`: ansible playbook for installing in a linux docker host.
- `nextcloud-docker-compose-gandi/`: ansible role used by the previous playbook
- `vars_sample.yml`: variables sample file. Se also role defaults and README.
- `nextcloud-docker/`: submodule refering to [nextcloud docker repo](https://github.com/nextcloud/docker/), used only for reference in the development.

Compose files organization

docker compose kit is organized in such that you can deploy without requiring Ansible.

- `current-compose/`
  - `docker-compose.yml`: docker-compose file
  - `certbot-dns-gandi-companion/`: companion container for [nginx-proxy/nginx-proxy](https://github.com/nginx-proxy/nginx-proxy), inspired on [nginx-proxy/nginx-proxy](https://github.com/nginx-proxy/nginx-proxy) used in nextcloud's docker examples.
  - `proxy-nginx/`: nginx proxy configuration based on nextcloud docker repo.
  - `docker-gen-ma-ng/`: [docker-gen](https://github.com/jwilder/docker-gen/releases) build process since there aren't releases for raspberry pi4.
  - `docker-gen-ma/`: [docker-gen](https://github.com/jwilder/docker-gen/releases) build process with goo complilation, since there weren't binaries for raspberry pi4. Now present for historical porpouses.
  - `nextcloud/`: nextcloud building image.
  - `app.env.example`: sample environment file
  - `companion.env.example`: sample environment file
  - `db.env.example`: sample environment file
  - `env.example`: sample environment file
  - `nginx.tmpl`: nginx template config adapted for docker-gen and certbot-dns-gandi-companion.
  - `README.md`: information relative to deployment with or without ansible.

# Issues

https://gitlab.com/matiaspecchia/nextcloud-docker-compose-acme-dns/-/issues
