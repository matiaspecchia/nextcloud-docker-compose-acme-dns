How to build:

- using buildkit

	COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1  docker-compose build 

- defining environment vars

	cat >> .env << EOF
	# archs for dockergen build
	DOCKERGEN_PREF_TARGETARCH=amd64
	DOCKERGEN_PREF_TAGETOS=linux
	EOF

	docker-compose build


- with ansible to a remote host:


	cp vars_sample.yml my_vars.yml
	editor my_vars.yml
	ansible-playbook -i host:port, -u developer install_nextcloud.yml  -e @my_vars.yml
