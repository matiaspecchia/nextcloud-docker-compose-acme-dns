certbot-dns-gandi-companion
---------------------------

Companion container for [nginx-proxy/nginx-proxy](https://github.com/nginx-proxy/nginx-proxy).

Automatic generation an renew of Let's Encrypt Certificates via DNS-01 Challenge.

```
+-----------------------------------------------------------+
|                        DNS  FQDN                          |
+--------------^------------------------^-------------------+
               |                        |
    +----------+-----+                  |
    |                |   +--------------+----------------+
    |     Gandi's    |   |                               |
    |   LiveDNS API  |   |      Let's Encrypt Service    |
    |                |   |                               |
    +---------^------+   +--^-+--------------------------+
              |             | |
+-----------------------------------------------------------+
|             |             | |                             |
|             |             | |            +-------------+  |
|    +--------+-------------+-v-+          |             |  |
|    |                          |          |             |  |
|    |       This Container     +--------->+             |  |
|    |                          |          |             |  |
|    +-----------^--------------+          |             |  |
|                |                         |  Docker API |  |
|                +-------------------------+             |  |
|                                          |             |  |
|    +--------------------------+          |             |  |
|    |                          |          |             |  |
|    |           APP            |          |             |  |
|    |                          |          |             |  |
|    +-----------^--------------+          |             |  |
|                |                         |             |  |
|                +-------------------------+             |  |
|                                          |             |  |
|                                          |             |  |
|   +---------------------------+          |             |  |
|   |                           |          |             |  |
|   |       Reverse proxy       |          +--+----------+  |
|   |    (wilder/nginx-proxy)   |             |             |
|   |                           |             |             |
|   +---------------^-----------+             |             |
|                   |                         |             |
|                   +-------------------------+             |
|                                                           |
+-----------------------------------------------------------+

```

Description
-----------

- on top of [certbot/certbot](https://hub.docker.com/r/certbot/certbot).
- Automatic generation and renewval via DNS-01 for Gandi LiveDNS API.
- Creates Diffie-Hellman Group at startup.
- inspired on [nginx-proxy/docker-letsencrypt-nginx-proxy-companion](https://github.com/nginx-proxy/docker-letsencrypt-nginx-proxy-companion).
- written for python and shell script.

Requirements
------------

- disposal of sharing the docker socket with this container.
- GandiDNS account.

Use
---

This container needs some variables for runnig and supports others optionals.
Many docker-letsencrypt-nginx-proxy-companion variables are implemented.


Current environment vars for this container (certbot-dns-gandi-companion).

- `RESTART_WHEN_RENEW`:  (true/false) restart reverse proxies containers.
                         Defaut: `true`.
- `ADHOC_DEPLOY_HOOK`: script to be executed as deploy hook (--deploy-hook).
                       Runs first than reverse proxy restarting.
                       Default: '/app/start.sh copy_to_jwilder_nginx-proxy'
- `PRE_HOOK`: script to be executed as pre hook (--pre-hook).
              Default: `None`.
- `POST_HOOK`: script to be executed as post hook (--post-hook).
               Default: `None`.
- `CERTBOT_OPTION`: additional certbot command options (ex
                    `CERTBOT_OPTION='--no-directory-hooks'`).
                    Default: `None`.
- `DHPARAM_BITS`: Diffie-Hellman key size.
                  Default: `2048`.
- `DEST_CERTS`: destination directory for certificates and dh file.
                Default: `/etc/nginx/certs`.
- `DH_FILE`: Diffie-Hellman destination file
             Default `${DEST_CERTS}/dhparam.pem`.
- `SLEEP`: infinite loop wait in seconds
           Default: `3600`.

The proxy container might have the following environment variables:

- `EXEC_CMD`: specific command to be triggered as exec.
       Default is `"['sh', '-c', '/app/docker-entrypoint.sh /usr/local/bin/docker-gen /app/nginx.tmpl /etc/nginx/conf.d/default.conf; /usr/sbin/nginx -s reload']"`


Supported enviroment vars for the app containers (to be read from)

- `LETSENCRYPT_HOST`: required.
- `VIRTUAL_HOST`: required.
- `LETSENCRYPT_EMAIL`: optional.
- `LETSENCRYPT_SINGLE_DOMAIN_CERTS`: optional.
- `LETSENCRYPT_TEST`: optional.
- `LETSENCRYPT_RESTART_CONTAINER`: restarts the app container if succesful renovation.

Requirement for reverse proxy container

- to be wilder/nginx-proxy or his heritage.
- to have the label `site.matiaspecchia.dev.certbot-dns01-companion.nginx_proxy`.

This environment vars are not implemented:
- `LETSENCRYPT_MIN_VALIDITY`.
- `LETSENCRYPT_KEYSIZE`.
- `REUSE_PRIVATE_KEYS`.
- `ACME_CA_URI`.
- `DEBUG`.
- `REUSE_ACCOUNT_KEYS`.
- `PROXY_CONT_LABEL`: label to be true for revere proxy containers.
                 is `"site.matiaspecchia.dev.certbot-dns01-companion.nginx_proxy"`).
- `DOCKERGEN_CONT_LABEL`: label to be true for docker-gen container
                 is `"site.matiaspecchia.dev.certbot-dns01-companion.docker_gen"`.
