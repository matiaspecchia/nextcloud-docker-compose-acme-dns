#!/bin/sh

# will generate an infinite loop for certificates update

SLEEP="${SLEEP:-3600}"

DH_DEFAULT_FILE="/app/dhparam.pem.default"
DEST_CERTS="${DEST_CERTS:-/etc/nginx/certs}"
DH_FILE="${DH_FILE:-${DEST_CERTS}/dhparam.pem}"
SHELL="/bin/sh"

DHPARAM_BITS=${DHPARAM_BITS:-"2048"}

ARG="${1}"

export ADHOC_DEPLOY_HOOK="${ADHOC_DEPLOY_HOOK:-${0} copy_to_jwilder_nginx-proxy}"

function dh_groups {

    if [ -f "${DH_FILE}" ];
    then
        if [ "$( md5sum ${DH_FILE} | awk '{print $1}' )" != "$( md5sum ${DH_DEFAULT_FILE} | awk '{print $1}' )" ];
        then
            # Diffie-Hellman groups file generated previously
            return 0
        fi
    else
        cp "${DH_DEFAULT_FILE}" "${DH_FILE}"
    fi

    echo "Generating DH groups file, this can take time..."
    openssl dhparam -out "${DH_FILE}.wip" "${DHPARAM_BITS}" 2>&1 && \
    mv "${DH_FILE}.wip" "${DH_FILE}" && \
    chmod 644 "${DH_FILE}" && \
    chown root:root "${DH_FILE}" && \
    ./docker_start restart --clabel site.matiaspecchia.dev.certbot-dns01-companion.nginx_proxy && \
    ./docker_start restart --clabel site.matiaspecchia.dev.certbot-dns01-companion.docker_gen
    return ${?}
}



# Copy certificates files for reverse proxy container (jwilder/nginx-proxy)
if [ "copy_to_jwilder_nginx-proxy" = "${ARG}" ]
then
    # $RENEWED_LINEAGE will point to the
    # config live subdirectory (for example,
    # "/etc/letsencrypt/live/example.com") containing the
    # new certificates and keys;
    # the shell variable
    # $RENEWED_DOMAINS will contain a space-delimited list
    # of renewed certificate domains (for example,
    # "example.com www.example.com"

    for domain in "${RENEWED_DOMAINS}"
    do
        echo "Copying new certificates for \"${domain}\"..."
        cp -L "${RENEWED_LINEAGE}/fullchain.pem" "${DEST_CERTS}/${domain}.crt"
        cp -L "${RENEWED_LINEAGE}/privkey.pem" "${DEST_CERTS}/${domain}.key"
        cp -L "${RENEWED_LINEAGE}/chain.pem" "${DEST_CERTS}/${domain}.chain.pem"

        rm -f "${DEST_CERTS}/${domain}.dhparam.pem"
        ln -s "${DH_FILE}" "${DEST_CERTS}/${domain}.dhparam.pem"

        chmod 644 "${DEST_CERTS}/${domain}.crt" "${DEST_CERTS}/${domain}.key" "${DEST_CERTS}/${domain}.chain.pem"
        chown root:root "${DEST_CERTS}/${domain}.crt" "${DEST_CERTS}/${domain}.key" "${DEST_CERTS}/${domain}.chain.pem"
    done
    exit 0
fi

dh_groups &


RENEW_OPTS=" "

if [ "_TRUE" = "_$(echo ${RESTART_WHEN_RENEW} | awk '{ print toupper($0) }')" ]
then
  RENEW_OPTS="${RENEW_OPTS} --restart-when-renew=yes"
fi

if [ ! "_" = "_${ADHOC_DEPLOY_HOOK}" ]
then
  RENEW_OPTS="${RENEW_OPTS} --adhoc-deploy-hook=\"${ADHOC_DEPLOY_HOOK}\""
fi

if [ ! "_" = "_${PRE_HOOK}" ]
then
  RENEW_OPTS="${RENEW_OPTS} --pre-hook=\"${PRE_HOOK}\""
fi

if [ ! "_" = "_${POST_HOOK}" ]
then
  RENEW_OPTS="${RENEW_OPTS} --post-hook=\"${POST_HOOK}\""
fi

if [ ! "_" = "_${CERTBOT_OPTION}" ]
then
  RENEW_OPTS="${RENEW_OPTS} --certbot-option=\"${CERTBOT_OPTION}\""
fi

while : ;
do
    # renew-cert can detect if the system is constructed with docker-gen or wilder/nginx-proxy containers
    # I have to call /bin/sh -c because in the expansion of ${RENEW_OPTS}
    #  the spaces are scaped with simple quotes
    ${SHELL} -c "$( echo "./docker_start renew-cert ${RENEW_OPTS}" )" && \
    sleep ${SLEEP}
done
